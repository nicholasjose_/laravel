<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="post">
    @csrf 
    <h3>Sign Up Form</h3>

    <label>First name:</label><br><br>
    <input type="text" name="namadepan"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="namabelakang"><br><br> 
    <label>Gender:</label><br><br>
        <input type="radio" id="Male" name="Gender"><label for="Male">Male</label>
            <br>
        <input type="radio" id="Female" name="Gender"><label for="female">Female</label>
            <br>
        <input type="radio" id="Other" name="Gender"><label for="Other">Other</label><br><br>
    <label>Nationality:</label><br><br>
    <select>
        <option>Indonesian</option>
        <option>Thailand</option>
        <option>USA</option>
        <option>China</option>
        <option>Japan</option>
    </select><br><br>
    <label>Language Spoken:</label><br><br>
        <input type="checkbox" id="Bahasa_Indonesia"><label for="Bahasa_Indonesia">Bahasa Indonesia</label>
            <br>
        <input type="checkbox" id="English"><label for="English">English</label>
            <br>
        <input type="checkbox" id="Other"><label for="Other">Other</label>
        <br><br>

    <label>Bio:</label><br><br>
    <textarea name="Bio" cols="30" rows="10"></textarea>
        <br>
    <input type="submit" value="Signup">
    </form>
</body>
</html>
<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class FormController extends Controller
{
    public function biodata()
    {
        return view('tugas1.form');
    }
    public function signup(Request $request)
    {
        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];
        return view('tugas1.welcome', compact('namadepan','namabelakang'));
    }
}
